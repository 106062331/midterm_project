function signin() {
    const email = document.getElementById("email");
    const password = document.getElementById("password");
    const signinBtn = document.getElementById("signinBtn");
    const googleBtn = document.getElementById("google-btn");

    signinBtn.addEventListener('click', function() {
        firebase.auth().signInWithEmailAndPassword(email.value, password.value).then(result => {
            window.location.href = 'chatroom.html';
        })
        .catch(function(error) { // Handle Errors here.
            alert(error.code);
            console.log(error.code);
        }); 
    });
    googleBtn.addEventListener('click', function() {
        var provider = new firebase.auth.GoogleAuthProvider();
        firebase.auth().signInWithPopup(provider).then(function(result) {
            var user = result.user;
            if(user){
                user.updateProfile({displayName: user.displayName, photoURL: "img/nouserpic.png"});
                let pushData = {
                    username: user.displayName,
                    email: user.email,
                    photoURL: "img/nouserpic.png"
                }
                firebase.database().ref('users/'+user.uid).set(pushData)
                .then(result=> {
                    window.location.href = 'chatroom.html';
                });
            }
            else{
                alert("user undefined");
            }
            window.location.href = 'chatroom.html';
            // ...
        }).catch(function(error) {
            var errorCode = error.code;
            var errorMessage = error.message;
            var email = error.email;
            alert(errorMessage);
        });
    });

}

function signup() {
    const email = document.getElementById("email");
    const password = document.getElementById("password");
    const username = document.getElementById("username");
    const signupBtn = document.getElementById("signup-btn");
    const gobackBtn = document.getElementById("gobackBtn");
    signupBtn.addEventListener('click', function() {
        // console.log(username.value);
        firebase.auth().createUserWithEmailAndPassword(email.value, password.value).then(()=>{
            let user = firebase.auth().currentUser;
            let uid = user.uid;
            if(user){
                user.updateProfile({displayName: username.value, photoURL: "img/nouserpic.png"});
                let pushData = {
                    username: username.value,
                    email: user.email,
                    photoURL: "img/nouserpic.png"
                }
                firebase.database().ref('users/'+uid).set(pushData)
                .then(result=> {
                    window.location.href = 'chatroom.html';
                });
            }
            else{
                alert("user undefined");
            }
        })
        .catch(function(error) {
            console.log(error.code);
            alert(error.code);
        });
        
    });
    gobackBtn.addEventListener('click', function() {
        window.location.href = 'index.html';
    })
    
}

function goSignUp() {
    window.location.href = "signup.html";
}