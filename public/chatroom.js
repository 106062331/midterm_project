const database = firebase.database();

function init() {
    firebase.auth().onAuthStateChanged(function(user) {
        if (user) {
            console.log(user);
            // User is signed in.
            initUser();
            initContact();
            addbyuid();
            addbymail();
            addbyusername();
            notify();
            changePhoto();
            // sendpic();
        } else {
            // No user is signed in.
        }
    });
}

function initContact() {
    let user = firebase.auth().currentUser;
    let userId = user.uid;

    let contactsul = document.getElementById("contactsul");
    let chatTo = "";
    contactsul.innerText = "";
    
    //(done)TODO: show contact
    database.ref('users/'+userId+'/friends').once('value').then((snapshot) => {
        let obj = snapshot.val();
        for(key in obj) {
            let liNode = document.createElement("li");
            let divNode = document.createElement("div");
            let imgNode = document.createElement("img");
            let div2Node = document.createElement("div");
            let pNode = document.createElement("p");
            let p1Node = document.createElement("p");
            liNode.classList.add("contact");
            liNode.id = key;
            divNode.classList.add("wrap");
            div2Node.classList.add("meta");
            pNode.classList.add("name");
            p1Node.classList.add("preview");
            p1Node.id = key + 'preview';
            database.ref('users/'+key).once('value').then((snap) => {
                imgNode.src = snap.child("photoURL").val();
                pNode.innerText = snap.child("username").val();
            });
            let previewMsgId = snapshot.child(key).val();
            database.ref('msg/' + previewMsgId).once('value').then((snap) => {
                if(snap.child("content").exists())  p1Node.innerText = snap.child("content").val();
                else if(snap.child("img").exists()) p1Node.innerText = "(picture)";
            });
            div2Node.appendChild(pNode);
            div2Node.appendChild(p1Node);
            divNode.appendChild(imgNode);
            divNode.appendChild(div2Node);
            liNode.appendChild(divNode)
            contactsul.appendChild(liNode);
            liNode.addEventListener('click', function handler() {
                if(chatTo != liNode.id){
                    showContent(liNode.id);
                    chatTo = liNode.id;
                }
                // liNode.removeEventListener('click', handler);
            });
        }
    });

    //(done): lobby contact event
    let lobby = document.getElementById("lobby");
    lobby.addEventListener('click', function() {
        if(chatTo != "LOBBY"){
            showContent("LOBBY");
            chatTo = "LOBBY";
        }
    });

    //(done): click event ==> newMessage
    let msg = document.getElementById("typetext");
    let sendBtn = document.getElementById("submit");
    let addpic = document.getElementById("addpic");
    sendBtn.addEventListener('click', function handler() {
        newMsg(chatTo, msg.value).then(() => {
            // sendBtn.removeEventListener('click', handler);
        }).catch((rej) => {
            console.log("newMsg err\nchatId: " + chatTo + "\nmsg: " + msg.value);
        });
        msg.value = "";
    });
    msg.addEventListener('keydown', function handler(e) {
        if(e.keyCode == 13){
            newMsg(chatTo, msg.value).then(() => {
                // msg.removeEventListener('click', handler);
            }).catch((rej) => {
                console.log("newMsg err\nchatId: " + chatTo + "\nmsg: " + msg.value);
            });
            msg.value = "";
        }
    });
    addpic.addEventListener('change', function handler(e) {
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function(event){
            sendpic(chatTo, event.target.result);
        }
    });

}

function showContent(chatId) {
    return new Promise((resolve, reject) => {
        if(chatId == "")  reject();
        let user = firebase.auth().currentUser;
        let userId = user.uid;
        let chatImg = document.getElementById("chat-img");


        //(done): name and photo
        database.ref('users/' + chatId).once('value').then((snapshot) => {
            let chatName = document.getElementById("chat-name");
            chatName.innerText = snapshot.child("username").val();
            chatImg.src = snapshot.child("photoURL").val();
        });

        //(done): show content
        let msgul = document.getElementById("msgul");
        msgul.innerText = "";
        let contentpos = (chatId == "LOBBY") ? 'chatList/LOBBY' : 'chatList/' + userId + '_' + chatId;
        database.ref(contentpos).on('child_added', (snapshot) => {
            let key = snapshot.key;
            let isSend = false;
            let liNode = document.createElement("li");
            let pNode = document.createElement("p");
            let timeNode = document.createElement("p");
            timeNode.classList.add("showedtext");
            liNode.id = key;

            if(chatId == "LOBBY" && snapshot.val() == userId || chatId != "LOBBY" && snapshot.val() == true)    isSend = true;
            else    isSend = false;
            if(isSend)  liNode.classList.add("send");
            else        liNode.classList.add("reply");
            
            database.ref('msg/' + key).once('value').then((snap) => {
                if(chatId == "LOBBY"){
                    database.ref('users/' + snap.child("sender").val() + '/username').once('value').then((snp) => {
                        timeNode.innerHTML = snap.child("time").val() + "<br>" + "by " + snp.val();
                    })
                }
                else    timeNode.innerText = snap.child("time").val();
                if(snap.child("content").exists()){
                    pNode.innerText = snap.child("content").val();
                    document.getElementById(chatId+'preview').innerText = snap.child("content").val();
                    if(!isSend){
                        let imgNode = document.createElement("img");
                        imgNode.src = chatImg.src;
                        if(chatId != "LOBBY")   liNode.appendChild(imgNode);
                    }
                    liNode.appendChild(pNode);
                    liNode.appendChild(timeNode);
                    msgul.appendChild(liNode);
                }
                else if(snap.child("img").exists()){
                    document.getElementById(chatId+'preview').innerText = "(picture)";
                    if(isSend)  liNode.innerHTML = "<img src=" + snap.child("img").val() + " class=" + "send-img" +" />";
                    else if(!isSend){
                        let imgNode = document.createElement("img");
                        imgNode.src = chatImg.src;
                        if(chatId != "LOBBY")   liNode.appendChild(imgNode);
                        liNode.innerHTML += "<img src=" + snap.child("img").val() + " class=" + "receive-img" +" />";
                    }
                    liNode.appendChild(timeNode);
                    msgul.appendChild(liNode);
                }
            }).then(() => {
                $(".messages").scrollTop($(".messages")[0].scrollHeight);
            });
            //(done): make contact to top
            
            
        });
        resolve();
    });
}

function newMsg(chatId, msg) {
    return new Promise((resolve, reject) => {
        if(chatId == "" || msg == "")   reject();
        else{
            let user = firebase.auth().currentUser;
            let userId = user.uid;
            
            //(done): update realtime db
            let date = new Date();
            let h = date.getHours();
            let m = date.getMinutes();
            let s = date.getSeconds();
            h = (h<10) ? '0'+h : h;
            m = (m<10) ? '0'+m : m;
            s = (s<10) ? '0'+s : s;
            let t = h + ':' + m + ':' + s;
            let pushData = {
                content: msg,
                sender: userId,
                receiver: chatId,
                time: t
            }
            let key = database.ref('msg').push().key;
            database.ref('msg/' + key).set(pushData);
            if(chatId == "LOBBY"){
                database.ref('chatList/LOBBY/' + key).set(userId);
                database.ref('users/LOBBY/latestmsg').set(key);
            }
            else{
                database.ref('chatList/' + userId + '_' + chatId + '/' + key).set(true);
                database.ref('chatList/' + chatId + '_' + userId + '/' + key).set(false);
                database.ref('users/' + userId + '/friends/' + chatId).set(key);
                database.ref('users/' + chatId + '/friends/' + userId).set(key);
            }

            //(done): update contact preview    //make updated contact to top
            document.getElementById(chatId+'preview').innerText = msg;

            // let msgul = document.getElementById("contactsul");
            // let newLi = document.createElement("li");
            // newLi.innerHTML = document.getElementById(chatId).innerHTML;
            // msgul.insertBefore(newLi, msgul.firstChild);
            // document.getElementById(chatId).remove();
            // newLi.classList.add("contact");
            // newLi.id = chatId;
            // newLi.addEventListener('click', function handler() {
            //     showContent(chatId).catch((rej) => console.log("show content err"));
            //     // newLi.removeEventListener('click', handler);
            // });
            resolve();
        }
    });
}

function popNotice() {
    return new Promise((resolve, reject) => {
        if (window.Notification) {
            if (Notification.permission == "granted") {
                new Notification("Dear " + firebase.auth().currentUser.displayName + " , new event!", {
                    body: "New message or new friend!",
                });
                resolve();
            }
            else if (Notification.permission != "denied") {
                Notification.requestPermission(function (permission) {
                    new Notification("Dear " + firebase.auth().currentUser.displayName + " , new event!", {
                        body: "New message or new friend!",
                    });
                    resolve();
                });
            }
            else{
                reject();
            }
        }
        console.log("POP NTF");
    });
}

function notify() {
    let user = firebase.auth().currentUser;
    let userId = user.uid;
    database.ref('users/' + userId + '/friends').on('child_changed', (snapshot) => {
        let newmsgId = snapshot.val();
        database.ref('msg/' + newmsgId + '/receiver').once('value').then((snap) => {
            if(snap.val() == userId)    popNotice();
        });
    });
}

function sendpic(chatId, imgUrl) {
    return new Promise((resolve, reject) => {
        if(chatId == "")   reject();
        else{
            let user = firebase.auth().currentUser;
            let userId = user.uid;

            //(done): update realtime db
            let date = new Date();
            let h = date.getHours();
            let m = date.getMinutes();
            let s = date.getSeconds();
            h = (h<10) ? '0'+h : h;
            m = (m<10) ? '0'+m : m;
            s = (s<10) ? '0'+s : s;
            let t = h + ':' + m + ':' + s;
            let pushData = {
                img: imgUrl,
                sender: userId,
                receiver: chatId,
                time: t
            }
            let key = database.ref('msg').push().key;
            database.ref('msg/' + key).set(pushData);
            if(chatId == "LOBBY"){
                database.ref('chatList/LOBBY/' + key).set(userId);
                database.ref('users/LOBBY/latestmsg').set(key);
            }
            else{
                database.ref('chatList/' + userId + '_' + chatId + '/' + key).set(true);
                database.ref('chatList/' + chatId + '_' + userId + '/' + key).set(false);
                database.ref('users/' + userId + '/friends/' + chatId).set(key);
                database.ref('users/' + chatId + '/friends/' + userId).set(key);
            }

            //TODO: update contact preview    //make updated contact to top
            document.getElementById(chatId+'preview').innerText = "(picture)";
            // let msgul = document.getElementById("contactsul");
            // let newLi = document.createElement("li");
            // newLi.innerHTML = document.getElementById(chatId).innerHTML;
            // msgul.insertBefore(newLi, msgul.firstChild);
            // document.getElementById(chatId).remove();
            // newLi.classList.add("contact");
            // newLi.id = chatId;
            // newLi.addEventListener('click', function handler() {
            //     showContent(newLi.id).catch((rej) => console.log("show content err"));
            // });
            resolve();
        }
    });

}

function changePhoto() {
    let profileImg = document.getElementById("change-profile-img");
    profileImg.addEventListener('change', function(e) {
        let reader = new FileReader();
        reader.readAsDataURL(e.target.files[0]);
        reader.onload = function(event){
            downscaleImage(event.target.result, 50);
        }
    });
}

function downscaleImage(dataUrl, newWidth) {
    let user = firebase.auth().currentUser;
    let userId = user.uid;

    // Create a temporary image so that we can compute the height of the downscaled image.
    let img = new Image();
    img.src = dataUrl;

    // Create a temporary canvas to draw the downscaled image on.
    let canvas = document.createElement("canvas");
    canvas.width = newWidth;
    canvas.height = newWidth;

    // Draw the downscaled image on the canvas and return the new data URL.
    let ctx = canvas.getContext("2d");
    img.onload = function () {
        ctx.drawImage(img, 0, 0, newWidth, newWidth);
        let newDataUrl = canvas.toDataURL();
        user.updateProfile({photoURL: newDataUrl}).then(() => {
            database.ref('users/' + userId + '/photoURL').set(newDataUrl);
            initUser();
        }).catch(() => downscaleImage(dataUrl, newWidth-1));
    }
}

function addbyuid() {
    let user = firebase.auth().currentUser;
    let userId = user.uid;
    let addUidBtn = document.getElementById('add-uid-btn');
    let addUidInput = document.getElementById('add-uid-input');
    addUidBtn.addEventListener('click', () => {
        let frienduid = addUidInput.value;
        database.ref('users').once('value').then((snapshot) => {
            database.ref('users/' + frienduid).once('value').then((snapshot) => {
                if(snapshot.exists()){
                    database.ref('users/' + userId + '/friends/' + frienduid).set(false);
                    database.ref('users/' + frienduid + '/friends/' + userId).set(false);
                    initContact();
                }
                else{
                    alert("user not exists!");
                }
            });
        }).then(() => {
            addUidInput.value = "";
        });
    });
}

function addbymail() {
    let user = firebase.auth().currentUser;
    let userId = user.uid;
    let addMailBtn = document.getElementById('add-mail-btn');
    let addMailInput = document.getElementById('add-mail-input');
    addMailBtn.addEventListener('click', () => {
        let friendmail = addMailInput.value;
        let flag = false;
        database.ref('users').once('value').then((snapshot) => {
            for(key in snapshot.val()) {
                if(snapshot.child(key).child("email").val() == friendmail){
                    let frienduid = key;
                    database.ref('users/' + userId + '/friends/' + frienduid).set(false);
                    database.ref('users/' + frienduid + '/friends/' + userId).set(false);
                    flag = true;
                    break;
                }
            }
        }).then(() => {
            if(!flag)   alert("user not exists!");
            addMailInput.value = "";
            initContact();
            location.reload();
        });

    });
}

function addbyusername() {
    let user = firebase.auth().currentUser;
    let userId = user.uid;
    let addUsernameBtn = document.getElementById('add-username-btn');
    let addUsernameInput = document.getElementById('add-username-input');
    addUsernameBtn.addEventListener('click', () => {
        let friendname = addUsernameInput.value;
        let flag = false;
        database.ref('users').once('value').then((snapshot) => {
            for(key in snapshot.val()) {
                if(snapshot.child(key).child("username").val() == friendname){
                    let frienduid = key;
                    database.ref('users/' + userId + '/friends/' + frienduid).set(false);
                    database.ref('users/' + frienduid + '/friends/' + userId).set(false);
                    flag = true;
                    break;
                }
            }
        }).then(() => {
            if(!flag)   alert("user not exists!");
            addUsernameInput.value = "";
            initContact();
            location.reload();
        });
    });
}

function initUser() {
    let user = firebase.auth().currentUser;
    let username = document.getElementById("username");
    username.innerText = user.displayName;
    let userPhoto = document.getElementById("profile-img");
    userPhoto.src = user.photoURL;
}

window.onload = function () {
    init();
}