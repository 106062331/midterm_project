# Software Studio 2019 Spring Midterm Project
## Notice
* Replace all [xxxx] to your answer

## Topic
* Project Name : [Chat Room]
* Key functions (add/delete)
    1. 登入
    2. 加入好友
    3. 聊天功能
    4. 儲存聊天記錄
    5. 多人聊天（設有LOBBY，所有的使用者，不分好友都能在這個聊天室聊天。）
    
    詳情請見Components Desicription
    
* Other functions (add/delete)
    1. 變更大頭貼
    2. 傳送照片
    3. 附上傳送時間

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Membership Mechanism|20%|Y|
|Firebase Page|5%|Y|
|Database|15%|Y|
|RWD|15%|Y|
|Topic Key Function|15%|Y|

## Advanced Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Third-Party Sign In|2.5%|Y|
|Chrome Notification|5%|Y|
|Use CSS Animation|2.5%|Y|
|Security Report|5%|Y|

## Website Detail Description

# 作品網址：[https://midtermproject-8cf5a.firebaseapp.com]

# Components Description : 

### 1. Sign in & Sign up: <br>
    用email位址及密碼可以註冊及登入，也可以使用google登入。


### 2. Function Description: <br>
* **init**: 
    網頁初始化。<br>
    
* **initUser**: 
    初始化左上角使用者資料（頭像、名字）。<br>
    
* **initContact**: 
    初始化左邊的聊天紀錄欄，並新增事件（點擊後就能和好友聊天）。<br>
    
* **showContent**: 
    按下左邊的聊天對象欄位（contact）後，右邊欄（content）就會列出訊息紀錄，包含對象的頭像及對話框。<br>
    
* **newMsg**: 
    將新增的訊息加入database，並更新content，contact中的preview會顯示最新的訊息。<br>
    
* **addbyusername/addbymail/addbyuid**: 
    由使用者名稱、email位址或使用者uid加入好友，加入好友後會更新contact，出現新好友。<br>
    
* **notify**: 
    在有新訊息或新好友的時候會跳出通知（**popNotice**）。<br>
    


### 2. Database :
* **users**
    * **user uid**
        * **username** : val
        * **email** : val
        * **photoURL**: val
        * **friends**: 
            * **friend 1 uid**: latest message id (default: false)
            * **friend 2 uid**: latest message id (default: false)
            * ... ...
* **chatList**
    * **(user uid)_(friend uid)**
        * **message 1 id**: bool (true: sent/ false: replied)
        * **message 2 id**: bool
        * ... ...
    * **(friend uid)_(user uid)**
        * **message 1 id**: bool
        * **message 2 id**: bool
        * ... ...
* **msg**
    * **message id 1**
        * **content（or img）**: val
        * **sender**: val
        * **receiver**: val
        * **time**: val
    * **message id 2**
        * **content（or img）**: val
        * **sender**: val
        * **receiver**: val
        * **time**: val
    * ... ...

    
### 6. Notification:

function notify() 在database "users/friends"有更動時會跳出通知，因為"users/friends/friends uid"中存的是最新訊息的message id，因此在有新好友和新訊息的時候會跳出通出。
    
### 3. Appearance & RWD:

[外觀參考原型](https://bootsnipp.com/snippets/exR5v)
**@media screen and (max-width: ...)** 設定RWD。
    
### 7. Animation:

在Sign in、Sign up頁面有css動畫。



# Other Functions Description(1~10%) : 
### 1. 變更大頭貼

* 點擊左上角使用者頭貼時可以選擇檔案，更換大頭貼，user.photoURL及database中users的資料兩者都會同時更新。
* 由於user.photoURL有大小的限制，因此在changePhoto中呼叫downscaleImage，將圖片縮小。在測試過程中發現，有些圖片縮小到48x48就能塞進user.photoURL，而有些圖片需要縮小到46x46。由於無法得知每張圖必須縮小到什麼程度，所以我設定為每張圖從50x50開始down scale，如果縮小後user.updateProfile失敗，就代表圖片URL還是過大，所以就再繼續縮小（downscaleImage(dataUrl, newWidth-1)），一直到大小符合user.photoURL的限制，才更新database中user的photoURL，並initUser()。而這個遞迴的過程可能會花一點時間，所以選擇圖片後需要等待數秒才會看到頭貼更新。
    * **changePhoto():** click event
    * **downscaleImage(dataUrl, newWidth):**
    首先create一個img element和canvas element，並將canvas設定為我們要的大小（newWidth），再把img放上canvas且設為我們想要的大小，這時canvas的url即為縮小後的url。
        
        

### 2. 傳送照片
相似於sendMsg，只是將database中msg的 **content:val** 改為 **img:val (image url)**，並在showContent中判斷要輸出圖片或文字。

### 3. 訊息附上傳送時間
收到及送出的訊息都標示傳送的時間，而LOBBY則會包含傳送者名字。

## Security Report (Optional)

在"rules"中將".read", ".write"設為"auth != null"，因此只有在登入的狀況下才能取用database資料。而因為資料庫的存法都是利用uid存取，要有uid才能取用到個人資料及聊天對話紀錄，登入的使用者只能取得自己及好友的uid，因此無法取到非好友的聊天紀錄。